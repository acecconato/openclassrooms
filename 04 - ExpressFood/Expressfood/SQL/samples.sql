-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 25 jan. 2018 à 02:07
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `oc_expressfood`
--

--
-- Déchargement des données de la table `status`
--

INSERT INTO `status` (`status`) VALUES
('disponible'),
('en attente'),
('en cours'),
('indisponible'),
('terminé');

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`categoryID`, `label`) VALUES
(2, 'dessert'),
(1, 'plat');

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`roleID`, `label`) VALUES
(1, 'customer'),
(2, 'deliveryman'),
(3, 'administration');

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`ingredientID`, `name`) VALUES
(7, 'caramel'),
(5, 'chocolat'),
(3, 'oeufs'),
(1, 'pates'),
(6, 'pomme'),
(2, 'saumon'),
(4, 'sucre');

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`userID`, `name`, `surname`, `creationDate`, `email`, `password`, `telephone`, `roleID`) VALUES
(1, 'Keelie', 'Torres', '2018-01-25', 'a.magna.Lorem@sedpedeCum.ca', 'IDK06PID3YW', '06 27 48 54 47', 1),
(2, 'Coby', 'Huffman', '2018-01-25', 'pellentesque@MorbimetusVivamus.edu', 'UIQ32NFO7CZ', '03 23 95 78 36', 1),
(3, 'Lavinia', 'Mayer', '2018-01-25', 'eu.metus.In@tellussemmollis.edu', 'MVY72ASY1SR', '05 74 35 43 60', 1),
(4, 'Nola', 'Higgins', '2018-01-25', 'cursus@enimmitempor.edu', 'YTA75ISC8YN', '06 97 10 68 36', 1),
(5, 'Sylvia', 'Hines', '2018-01-25', 'Nulla.tincidunt.neque@utdolor.org', 'ZNI81RQJ2QN', '06 19 35 64 13', 1),
(6, 'Susan', 'Willis', '2018-01-25', 'ante.ipsum.primis@parturient.co.uk', 'VFP47RCV1OF', '09 34 36 83 70', 1),
(7, 'Odette', 'Pope', '2018-01-25', 'egestas.urna@amet.org', 'ULM34YED2XC', '08 17 45 05 57', 1),
(8, 'Colorado', 'Huff', '2018-01-25', 'Vivamus.molestie.dapibus@enimcondimentum.ca', 'DLC82UAL0FI', '03 99 73 05 77', 1),
(9, 'Mark', 'Sims', '2018-01-25', 'hymenaeos@nonjustoProin.co.uk', 'XPK11HAM3MZ', '04 70 21 01 81', 1),
(10, 'Phelan', 'Goodman', '2018-01-25', 'cursus@parturientmontes.org', 'LAI86BTH7VG', '06 39 18 26 80', 1),
(11, 'Ursa', 'Nelson', '2018-01-25', 'ipsum.Suspendisse@eratEtiamvestibulum.edu', 'GYJ27IRM9FF', '05 71 72 86 10', 2),
(12, 'Rudyard', 'Henson', '2018-01-25', 'arcu@nuncsedlibero.edu', 'ZYU52FYL2VE', '09 34 64 87 17', 2),
(13, 'Cherokee', 'Anthony', '2018-01-25', 'tristique.ac.eleifend@egestasligula.co.uk', 'OXI35UCW0YL', '08 53 83 11 65', 2),
(14, 'Orla', 'Ray', '2018-01-25', 'ad@ligulaelitpretium.edu', 'VQF79VVJ4FU', '08 68 25 20 48', 2),
(15, 'Wesley', 'Knowles', '2018-01-25', 'conubia.nostra.per@miAliquam.edu', 'ZQV29RGT0KB', '02 93 38 77 33', 2),
(16, 'Tamekah', 'Duran', '2018-01-25', 'eu.odio.tristique@maurisut.co.uk', 'SOY47KHH2YC', '07 21 00 52 68', 3),
(17, 'Ian', 'Bryant', '2018-01-25', 'sem.egestas.blandit@Donecfringilla.net', 'PZY22KVZ3SP', '03 88 86 83 75', 3),
(18, 'Christen', 'Ramirez', '2018-01-25', 'mus.Aenean.eget@Inornare.edu', 'KAU43ITV9NX', '06 38 80 52 36', 3);

--
-- Déchargement des données de la table `adress`
--

INSERT INTO `adress` (`adressID`, `country`, `city`, `adress`, `zipCode`, `longitude`, `latitude`, `userID`) VALUES
(1, 'France', 'Charleville-Mézières', 'CP 543, 1422 Senectus Rd.', 50590, -115.558, -16.5769, 1),
(2, 'France', 'Belfort', 'CP 633, 6816 Odio Rd.', 67455, 34.7856, 56.4192, 2),
(3, 'France', 'Saintes', '611 Fusce Av.', 94976, -121.118, 70.3796, 3),
(4, 'France', 'Gap', 'Appartement 855-7670 Eu Avenue', 82533, -162.209, 67.8338, 4),
(5, 'France', 'Auxerre', '4610 Tincidunt Av.', 36743, 173.589, 56.6713, 5),
(6, 'France', 'La Seyne-sur-Mer', 'CP 435, 5122 Neque. Chemin', 88274, -117.958, 58.9574, 6),
(7, 'France', 'Saint-Dizier', '929-5618 Nunc Chemin', 46378, -169.775, 35.6175, 7),
(8, 'France', 'Orvault', 'Appartement 632-4445 Sed Route', 63734, -85.1044, -30.1638, 8),
(9, 'France', 'Strasbourg', '295-257 Aenean Rue', 45277, -23.1854, 46.5006, 9),
(10, 'France', 'Rennes', '5869 Lacinia Impasse', 34316, 48.2009, 8.57814, 10);

--
-- Déchargement des données de la table `creditcard`
--

INSERT INTO `creditcard` (`numero`, `cardholderName`, `expiryDate`, `cryptogram`) VALUES
(487895423578954, 'Mayer Lavinia', '2018-05-11', 916),
(1244628465458094, 'Willis Susan', '2022-12-10', 302),
(1389646548784466, 'Brock Deanna', '2020-04-29', 901),
(1454409611547146, 'Huffman Coby', '2021-09-19', 354),
(2418079874654125, 'Graham Chastity', '2018-05-02', 833),
(2610994205756214, 'Torres Keelie', '2019-11-14', 777),
(4565858521212478, 'Torres Keelie', '2019-11-14', 777),
(5555655698545781, 'Huffman Coby', '2021-09-19', 354),
(6169886514684672, 'Higgins Nola', '2021-10-23', 408),
(6169886874654672, 'Spence Timon', '2020-06-11', 268),
(6169889841464672, 'Mayer Lavinia', '2018-05-11', 916),
(6365488894321321, 'Nunez Emerson', '2020-08-21', 764),
(8084468741654162, 'Hines Sylvia', '2020-04-13', 678);

--
-- Déchargement des données de la table `deliveryman`
--

INSERT INTO `deliveryman` (`dmID`, `longitude`, `latitude`, `userID`, `status`) VALUES
(1, 0, 0, 11, 'disponible'),
(2, 0, 0, 12, 'disponible'),
(3, 0, 0, 13, 'indisponible'),
(4, 0, 0, 14, 'indisponible'),
(5, 0, 0, 15, 'indisponible');

--
-- Déchargement des données de la table `dish`
--

INSERT INTO `dish` (`dishID`, `name`, `price`, `description`, `creationDate`, `picture`, `categoryID`) VALUES
(1, 'Pates au saumon', '10.00', NULL, '2018-01-25', 'img/pate_saumon', 1),
(2, 'pates aux oeufs', '6.00', NULL, '2018-01-25', 'img/pates_oeufs', 1),
(3, 'Tarte au chocolat', '9.00', NULL, '2018-01-25', 'img/tarte_chocolat', 2),
(4, 'Pomme caramel', '7.00', NULL, '2018-01-25', 'img/pomme_caramel', 2);

--
-- Déchargement des données de la table `dish_deliveryman`
--

INSERT INTO `dish_deliveryman` (`quantity`, `dmID`, `userID`, `dishID`) VALUES
(5, 1, 11, 1),
(5, 1, 11, 2),
(5, 1, 11, 3),
(5, 1, 11, 4),
(4, 2, 12, 1),
(4, 2, 12, 2),
(4, 2, 12, 3),
(4, 2, 12, 4),
(3, 3, 13, 1),
(3, 3, 13, 2),
(3, 3, 13, 3),
(3, 3, 13, 4),
(2, 4, 14, 1),
(2, 4, 14, 2),
(2, 4, 14, 3),
(2, 4, 14, 4),
(1, 5, 15, 1),
(1, 5, 15, 2),
(1, 5, 15, 3),
(1, 5, 15, 4);

--
-- Déchargement des données de la table `dish_ingredient`
--

INSERT INTO `dish_ingredient` (`dishID`, `ingredientID`) VALUES
(1, 1),
(2, 1),
(1, 2),
(2, 3),
(3, 4),
(3, 5),
(4, 6),
(4, 7);

--
-- Déchargement des données de la table `history`
--

INSERT INTO `history` (`historyDate`, `activationStatus`, `dishID`) VALUES
('2018-01-25', 1, 1),
('2018-01-25', 1, 2),
('2018-01-25', 1, 3),
('2018-01-25', 1, 4);

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`orderID`, `creationDate`, `dmID`, `userID`, `status`) VALUES
(1, '2018-01-25', NULL, 1, 'en attente'),
(2, '2018-01-25', 1, 2, 'en cours');

--
-- Déchargement des données de la table `orders_user_adress`
--

INSERT INTO `orders_user_adress` (`userID`, `orderID`, `adressID`) VALUES
(1, 1, 1),
(2, 2, 2);

--
-- Déchargement des données de la table `order_dish`
--

INSERT INTO `order_dish` (`quantity`, `orderID`, `dishID`) VALUES
(2, 1, 1),
(3, 1, 4),
(4, 2, 2),
(1, 2, 3);

--
-- Déchargement des données de la table `user_creditcard`
--

INSERT INTO `user_creditcard` (`numero`, `userID`) VALUES
(1389646548784466, 1),
(2610994205756214, 1),
(4565858521212478, 1),
(1454409611547146, 2),
(2418079874654125, 3),
(6169889841464672, 3),
(6169886514684672, 4),
(6169886874654672, 5),
(1244628465458094, 6),
(5555655698545781, 7);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
